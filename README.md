# `tfenv` in docker

Allow you to manage terraform versions like a `Pro` within your CI pipeline.

[tfenv](https://github.com/tfutils/tfenv) does a few really neat things:

* allows you to install multiple versions of Terraform in parallel;
* allows you to pick the one you want to use;
* allows you to drop a file next to your Terraform code to specify which version of Terraform it needs;
