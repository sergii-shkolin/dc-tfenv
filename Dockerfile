FROM alpine:3.10.1

ENV VERSION=v1.0.1
ENV PATH="/tfenv/bin:$PATH"

RUN apk add --update --no-cache git curl bash \
    && git clone -b ${VERSION} --single-branch --depth 1 https://github.com/tfutils/tfenv.git /tfenv

ENTRYPOINT [ "/tfenv/bin/tfenv" ]
